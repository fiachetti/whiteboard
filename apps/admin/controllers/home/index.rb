module Admin
  module Controllers
    module Home
      class Index
        include Admin::Action

        expose :courses
        
        def call(params)
          @courses = CourseRepository.new.all
        end
      end
    end
  end
end
