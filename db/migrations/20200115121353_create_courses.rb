Hanami::Model.migration do
  change do
    create_table :courses do
      primary_key :id

      column :slug,        String, null: false, unique: true
      column :title,       String, null: false
      column :description, String

      column :published_at, DateTime
      
      column :created_at, DateTime, null: false
      column :updated_at, DateTime, null: false
    end
  end
end
