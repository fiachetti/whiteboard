class Course < Hanami::Entity
  def published?
    !! published_at
  end

  def draft?
    ! published?
  end
end
