RSpec.describe Admin::Controllers::Home::Index do
  let(:action) { described_class.new }
  let(:params) { Hash[] }
  let(:repository) { CourseRepository.new }

  before do
    repository.clear
  end

  it "is successful" do
    response = action.call(params)

    expect(response[0]).to eql(200)
  end

  it "exposes all the courses" do
    @course1 = repository.create(title: "course1", slug: "course1")
    @course2 = repository.create(title: "course2", slug: "course2")

    action.call(params)
    
    expect(action.exposures[:courses]).to match_array([@course1, @course2])
  end
end
