require "features_helper"

RSpec.describe "Show admin courses" do
  let(:repository) { CourseRepository.new }

  before do
    repository.clear
  end
  
  it "is successful" do
    visit "/admin"

    expect(page).to have_css(".Admin")
  end

  it "shows all the courses" do
    repository.create(title: "TDD", slug: "tdd")
    repository.create(title: "Ruby on Rails", slug: "rails")
    
    visit "/admin"

    expect(page).to have_css(".Admin__courses", count: 1)
    expect(page).to have_css(".Admin__course", count: 2)
  end

  it "shows draft batch if the course hasn't been published" do
    repository.create(title: "TDD", slug: "tdd")
    
    visit "/admin"

    expect(page).to have_css(".Badge.Badge__gray", text: "Draft")
  end

  it "shows published batch if the course has been published" do
    repository.create(title: "TDD", slug: "tdd", published_at: DateTime.now)
    
    visit "/admin"

    expect(page).to have_css(".Badge.Badge__success", text: "Published")
  end

end
