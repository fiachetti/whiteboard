RSpec.describe Course, type: :entity do
  it 'can be initialized with attributes' do
    course = Course.new(title: "TDD", slug: "tdd", description: "Test Driven Development")

    expect(course.title).to eql("TDD")
    expect(course.slug).to eql("tdd")
    expect(course.description).to eql("Test Driven Development")
  end

  it "is draft" do
    course = Course.new(title: "TDD", slug: "tdd")

    expect(course.published?).to eql(false)
    expect(course.draft?).to eql(true)
  end
  
  it "is published" do
    course = Course.new(title: "TDD", slug: "tdd", published_at: DateTime.now)

    expect(course.published?).to eql(true)
    expect(course.draft?).to eql(false)
    
  end
end
